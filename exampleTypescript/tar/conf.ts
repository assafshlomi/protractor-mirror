// CDD TAR
// conf suites file with all ts specs
import {Config} from 'protractor';

export let config: Config = {
  framework: 'jasmine',
  capabilities: {
    browserName: 'chrome'
  },
  
suites: {
        "Test-Payment-AP": '../tar/00CAW-spec-payment-AP.ts',
	"Test-Payment-M": '../tar/00CAW-spec-payment-M.ts',
	"Test-Users-M": '../tar/00CAW-spec-users-M.ts',
	"Test-Login-AP": '../tar/00CAW-spec.login-AP.ts',
	"Test-Users-AF": '../tar/00CAW-spec.users-AF.ts'
	},

  seleniumAddress: 'http://localhost:4444/wd/hub',

  // You could set no globals to true to avoid jQuery '$' and protractor '$'
  // collisions on the global namespace.
  noGlobals: true
};
